import {Component, OnInit} from '@angular/core';
import {Task} from "../task.model";
// import {Event} from "@angular/router";
import {TaskService} from "../task.service";

@Component({
    selector: 'app-tasks-list',
    templateUrl: './tasks-list.component.html',
    styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

    constructor(private taskService: TaskService) {
    }

    tasks: Task[] = [];

    ngOnInit(): void {
        this.taskService.getTasks()
            .subscribe(
                (tasks: any) => {
                    this.tasks = tasks
                }, (error => console.log(error))
            );

        this.taskService.onTaskAdded.subscribe((task:Task)=> {this.tasks.push(task)});

        /*this.tasks.push(new Task(1, "Listen to Hello from Adele", true, "04/12/2020"));
        this.tasks.push(new Task(2, "Learn springboot", true, "06/12/2020"));
        this.tasks.push(new Task(3, "Attend to SAP course", false, "07/12/2020"));
        this.tasks.push(new Task(4, "Buy some staffs in the market", true, "09/12/2021"));*/

    }

    onTaskChange(event: Event, task: Task) {
        console.log(task.name + ' has changed');
     ;
        return this.taskService.saveTask(task,(<HTMLInputElement>event.target).checked).subscribe();
    }

    getDueDateLabel(task: Task) {
        return task.completed ? 'label-success' : 'label-primary';
    }


}
