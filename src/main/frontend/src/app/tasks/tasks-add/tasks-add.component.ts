import {Component, OnInit} from '@angular/core';
import {TaskService} from "../task.service";
import {Task} from "../task.model";

@Component({
    selector: 'app-tasks-add',
    templateUrl: './tasks-add.component.html',
    styleUrls: ['./tasks-add.component.css']
})
export class TasksAddComponent implements OnInit {
    addTaskValue: String = "";

    constructor(private taskService: TaskService) {
    }

    ngOnInit(): void {
    }

    onTaskAdd(event: Event) {

      this.taskService.addTask(new Task((<HTMLInputElement>event.target).value, false, this.getTodayAsString()))
        .subscribe(
            (newTask: any) => {
                this.addTaskValue = " ";
                this.taskService.onTaskAdded.emit(newTask);
            }
        )
      ;


    }

    getTodayAsString() {
        let today = new Date();
        let dd: any = today.getDate();
        let mm: any = today.getMonth();
        let yyyy: any = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        return mm + '/' + dd + '/' + yyyy;
    }
}

