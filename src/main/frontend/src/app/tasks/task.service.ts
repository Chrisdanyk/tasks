import {EventEmitter, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map} from 'rxjs/operators';
import {Task} from "./task.model";

@Injectable()
export class TaskService {

    constructor(private http: HttpClient) {
    }

    onTaskAdded = new EventEmitter<Task>();

    getTasks() {
        return this.http.get('/api/tasks');
    }

    saveTask(task: Task, checked: boolean) {
        task.completed = checked;
        return this.http.post('/api/tasks/save', task);
    }

    addTask(task: Task) {
        return this.http.post('/api/tasks/save', task);
    }
}

