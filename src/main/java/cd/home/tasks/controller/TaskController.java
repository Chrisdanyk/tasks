package cd.home.tasks.controller;

import cd.home.tasks.domain.Task;
import cd.home.tasks.service.TaskServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
@CrossOrigin(origins = "http://localhost:8080/api/tasks")
@RestController
@RequestMapping("/api/tasks")
public class TaskController {
    @Autowired
    private TaskServiceImpl taskServiceImpl;

    @GetMapping(value = {"", "/"})
    public Iterable<Task> list() {
        return taskServiceImpl.list();
    }

    @PostMapping("/save")
    public Task saveTask(@RequestBody Task task) {
        return taskServiceImpl.save(task);
    }
}
