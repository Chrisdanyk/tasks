package cd.home.tasks.service;

import cd.home.tasks.domain.Task;

public interface TaskService {
    Iterable<Task> list();
    Task save(Task task);
}
