package cd.home.tasks;

import cd.home.tasks.domain.Task;
import cd.home.tasks.service.TaskService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@SpringBootApplication
public class TasksApplication {

    public static void main(String[] args) {
        SpringApplication.run(TasksApplication.class, args);
    }

   /* @Bean
    CommandLineRunner runner(TaskService taskService) {
        return args -> {
            taskService.save(new Task(1L, "Create SpringBoot Application", LocalDate.now(), Boolean.TRUE));
            taskService.save(new Task(2L, "Create Spring Project  Packages", LocalDate.now().plus(1, ChronoUnit.DAYS), Boolean.TRUE));
            taskService.save(new Task(3L, "Create The Task Domain class", LocalDate.now().plus(2, ChronoUnit.DAYS), Boolean.TRUE));
            taskService.save(new Task(4L, "Create Service and repository  classes", LocalDate.now().plus(3, ChronoUnit.DAYS), Boolean.TRUE));
            taskService.save(new Task(5L, "Check H2 console for initial data", LocalDate.now().plus(4, ChronoUnit.DAYS), Boolean.TRUE));

        };
    }*/

}
